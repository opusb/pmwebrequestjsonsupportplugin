<?php
class pmWebRequestJson  {
    public static function readJson(sfEvent $event) {
        /** @var sfWebRequest $request */
        $request = $event->getSubject();
        if ('application/json' === $request->getContentType() && $content = $request->getContent()) {
            if ($json = json_decode($request->getContent())) return $json;
            else return array();
        }
        return array();
    }
}