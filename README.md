# README #

This plugin provides access to JSON parameters sent in request payload
via **sfWebRequest::getParameter()**

It's usefull for **AngularJS** backend application to read parameters sent with **$http()** method.

Plugin works with [Symfony 1.4](http://symfony.com/legacy)

### Installing plugin ###

* Clone this repository to **/plugins** folder of your project
* Add plugin to configuration file **/config/ProjectConfiguration.class.php**

```
#!php
<?php
class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    $this->enablePlugins('pmWebRequestJsonSupportPlugin');
  }
}

```

### Using plugin ###
```
#!javascript
/** angularJSApp/app.js */
$http.post('/api/newUser',{name: 'Piotr', surname: 'Mocek'});
```

```
#!php
<?php
/** /apps/frontend/modules/api/actions/actions.class.php **/
class apiActions extends sfActions {
    public function executeNewUser(sfWebRequest $request) {
        $this->user = array();
        $this->user['name'] = $request->getParameter('name');
        $this->user['surnamename'] = $request->getParameter('surnamename');
    }
}
```