<?php

/**
 * Created by PhpStorm.
 * User: piotr.mocek
 * Date: 23.02.2017
 * Time: 14:52
 */
class pmWebRequestJsonSupportPluginConfiguration extends sfPluginConfiguration
{
    public function initialize() {
        $this->dispatcher->connect('request.filter_parameters', array('pmWebRequestJson', 'readJson'));
    }
}